Q: What do you have on Kain's Private Repo?
A: Unreleased scripts, new versions of scripts, and scripts that I may never release for whatever reason.

Q: How do I get access to Kain's Private Repo?
A: Send a $20 donation via the Donate button in my signature on the forum. Send is as a gift on PayPal and include your BoL username in the comments. Then PM me your BitBucket username. I'll add you to the repo when I'm online next.

Q: I already donated at least $20. How do I get access?
A: PM me.

Q: What's on your Private Repo?
A: See https://bitbucket.org/KainBoL/bol/src/master/What%27s%20New%20on%20Private%20Repo.txt

Q: What's the link to the Private Repo?
A: https://bitbucket.org/KainBoL/bol-private

Q: What are the "Terms of Use/Service"?
A: By donating, you understand that your donation is actually a donation, and comes without obligation of services.
As a "gift", you will gain access to the private repo, where you understand that access to the private repo is primarily for early access to new scripts, new versions of scripts, and features that may sometime in the future be made public.
While Kain generally helps users who need help with scripts, and sometimes even adds features on request, you understand that there is no obligation that Kain must do so.

-- Kain